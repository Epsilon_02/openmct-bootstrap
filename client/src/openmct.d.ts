// Type definitions for openmct
declare module 'openmct' {
  export function setAssetPath(path: string): void;
  export function install(plugins: object): void;
  export function start(): void;

  export namespace plugins {
    export function UTCTimeSystem(): object;
    export function RemoteClock(): object;
    export function LocalTimeSystem(): object;
    export function ISOTimeFormat(): object;
    export function MyItems(): object;
    export function GeneratorPlugin(): object;
    export function EventGeneratorPlugin(): object;
    export function AutoflowPlugin(): object;
    export function TimeConductorPlugin(): object;
    export function ExampleImagery(): object;
    export function ImageryPlugin(): object;
    export function SummaryWidget(): object;
    export function URLIndicatorPlugin(): object;
    export function TelemetryMean(): object;
    export function PlotPlugin(): object;
    export function ChartPlugin(): object;
    export function TelemetryTablePlugin(): object;
    export function StaticRootPlugin(): object;
    export function Notebook(): object;
    export function DisplayLayoutPlugin(): object;
    export function FormActions(): object;
    export function FolderView(): object;
    export function FlexibleLayout(): object;
    export function Tabs(): object;
    export function LADTable(): object;
    export function Filters(): object;
    export function ObjectMigration(): object;
    export function GoToOriginalAction(): object;
    export function OpenInNewTabAction(): object;
    export function ClearData(): object;
    export function WebPagePlugin(): object;
    export function ConditionPlugin(): object;
    export function ConditionWidgetPlugin(): object;
    export function Espresso(): object;
    export function Maelstrom(): object;
    export function Snow(): object;
    export function URLTimeSettingsSynchronizer(): object;
    export function NotificationIndicator(): object;
    export function NewFolderAction(): object;
    export function NonEditableFolder(): object;
    export function CouchDBPlugin(): object;
    export function DefaultRootName(): object;
    export function PlanLayout(): object;
    export function ViewDatumAction(): object;
    export function ViewLargeAction(): object;
    export function ObjectInterceptors(): object;
    export function PerformanceIndicator(): object;
    export function CouchDBSearchFolder(): object;
    export function Timeline(): object;
    export function Hyperlink(): object;
    export function Clock(): object;
    export function DeviceClassifier(): object;
    export function Timer(): object;
    export function UserIndicator(): object;
    export function ExampleUser(): object;
    export function LocalStorage(): object;
  }

  export namespace time {
    export function clock(key: string, value: any): void;
    export function timeSystem(timeSystemOrKey: string): void;
  }
}
