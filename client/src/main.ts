import './style.css';
import '../node_modules/openmct/dist/espressoTheme.css'; // because of openmct you have to import the theme here to bundle it -.-
import openmct from 'openmct';

openmct.setAssetPath('node_modules/openmct/dist');
openmct.install(openmct.plugins.LocalStorage());
openmct.install(openmct.plugins.MyItems());
openmct.install(openmct.plugins.UTCTimeSystem());
openmct.time.clock('local', { start: -15 * 60 * 1000, end: 0 });
openmct.time.timeSystem('utc');

openmct.start();

// const app = document.querySelector<HTMLDivElement>('#app')!;

// app.innerHTML = `
//   <h1>Hello Vite!</h1>
//   <a href="https://vitejs.dev/guide/features.html" target="_blank">Documentation</a>
// `;
