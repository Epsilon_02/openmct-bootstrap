// eslint-disable-next-line import/no-extraneous-dependencies
import { defineConfig } from 'vite';
import path from 'path';

export default defineConfig({
  resolve: {
    alias: {
      openmct: path.resolve(__dirname, './node_modules/openmct/dist/openmct'),
    },
  },
});
